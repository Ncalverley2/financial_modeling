#######################################################################################
## LOAD THE FULL LIST OF TICKERS AND METRICS
#######################################################################################

load("master/all_tickers.RData")
metrics <- read.csv("master/stock_metrics.csv", stringsAsFactors = FALSE)

#######################################################################################
## LOOP THROUGH ALL TICKERS AND DOWNLOAD THEIR DATA
#######################################################################################

for(idx in tickers$ticker) {
  
  #######################################################################################
  ## IMPORT THE YAHOO DATA
  #######################################################################################
  yahoo_data <- scrape_yahoo_data(ticker = idx, mets = metrics)
  
  #######################################################################################
  ## CLEAN THE YAHOO DATA
  #######################################################################################
  yahoo_data <- clean_yahoo_finance(fulldata = yahoo_data)
  
  #######################################################################################
  ## IMPORT MORNINGSTAR DATA
  #######################################################################################
  morningstar_data <- scrape_morningstar_data(ticker = idx, mets = metrics)
  
  #######################################################################################
  ## MERGE ALL DATA
  #######################################################################################
  tempdata <- merge(yahoo_data, morningstar_data, by = "ticker"); rm(yahoo_data, morningstar_data)
  
  #######################################################################################
  ## BIND THE DATABASE DATA TOGETHER
  #######################################################################################
  
  ## Bind onto the full data set
  if(!exists("fulldata")) {
    fulldata <- tempdata; rm(tempdata)
  } else {
    fulldata <- rbind.fill(fulldata, tempdata); rm(tempdata)
  }
  
  ## Wait 5 seconds before moving onto the next ticker
  Sys.sleep(5)
  
}

#######################################################################################
## SAVE THE DATA
#######################################################################################

dir.create(path = paste0("data/", gsub("-", "_", Sys.Date())))
save(fulldata, file = paste0("data/", gsub("-", "_", Sys.Date()), "/imported_data.RData"))

#######################################################################################
## CLEAN UP
#######################################################################################

rm(fulldata, idx, metrics, tickers, ticker)











