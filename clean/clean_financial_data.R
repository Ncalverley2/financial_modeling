#######################################################################################
## LOAD THE DATA
#######################################################################################

load(file = paste0("data/", gsub("-", "_", Sys.Date()), "/imported_data.RData"))

#######################################################################################
## CLEAN THE DATA
#######################################################################################

fulldata <- within(fulldata, {
  
  ## Calculate the stock price
  price <- NA
  price[!is.na(ask)] <- ask[!is.na(ask)]
  price[is.na(ask)] <- bid[is.na(ask)]
  
  ## Put in a capitalization size indicator
  market_cap_category <- NA
  market_cap_category[!is.na(market_capitalization) & market_capitalization >= 10000000000] <- "Large"
  market_cap_category[!is.na(market_capitalization) & market_capitalization < 10000000000] <- "Mid"
  market_cap_category[!is.na(market_capitalization) & market_capitalization < 2000000000] <- "Small"
  
})
  
# ## Calculate the 52 week range
# fulldata$year_price_range <- fulldata$"52_week_high" - fulldata$"52_week_low"
#   
# ## Calculate price as a pct of the 52 week range
# fulldata$price_as_pct_of_year_range <- (fulldata$price - fulldata$"52_week_low") / fulldata$year_price_range
# fulldata$price_as_pct_of_year_range[!is.finite(fulldata$price_as_pct_of_year_range)] <- NA


#######################################################################################
## PUT IN FLAGS FOR KEY FINANCIAL METRICS
#######################################################################################

fulldata <- within(fulldata, {
  
  revenue_flag <- NA
  revenue_flag[!is.na(revenue_yoy_3yr_avg) & revenue_yoy_3yr_avg > 0] <- 1
  revenue_flag[is.na(revenue_flag)] <- 0
  
  eps_trend_flag <- NA
  eps_trend_flag[!is.na(eps_3yr_avg) & eps_3yr_avg > 0] <- 1
  eps_trend_flag[is.na(eps_trend_flag)] <- 0
  
  eps_projected_flag <- NA
  eps_projected_flag[!is.na(eps_estimate_next_year) & eps_estimate_next_year > 0 &
                     !is.na(eps_3yr_avg) & eps_estimate_next_year > eps_3yr_avg] <- 1
  eps_projected_flag[is.na(eps_projected_flag)] <- 0 
  
  peg_flag <- NA
  peg_flag[!is.na(peg_ratio) & peg_ratio < 1.0] <- 1
  peg_flag[is.na(peg_flag)] <- 0
  
  short_interest_flag <- NA
  short_interest_flag[!is.na(short_ratio) & short_ratio < 1.0] <- 1
  short_interest_flag[is.na(short_interest_flag)] <- 0
  
  price_book_ratio_flag <- NA
  price_book_ratio_flag[!is.na(price_per_book) & price_per_book < 1] <- 1
  price_book_ratio_flag[is.na(price_book_ratio_flag)] <- 0
  
  price_earnings_ratio_flag <- NA
  price_earnings_ratio_flag[!is.na(price_earnings_ratio) & price_earnings_ratio < 15] <- 1
  price_earnings_ratio_flag[is.na(price_earnings_ratio_flag)] <- 0
  
  dividend_yield_flag <- NA
  dividend_yield_flag[!is.na(dividend_yield) & dividend_yield > 3] <- 1
  dividend_yield_flag[is.na(dividend_yield_flag)] <- 0
  
  free_cash_flow_growth_flag <- NA
  free_cash_flow_growth_flag[!is.na(free_cash_flow_growth_pct_yoy) & free_cash_flow_growth_pct_yoy > 100] <- 1
  free_cash_flow_growth_flag[is.na(free_cash_flow_growth_flag)] <- 0
  
  debt_equity_ratio_flag <- NA
  debt_equity_ratio_flag[!is.na(debt_equity_ratio_3yr_avg) & debt_equity_ratio_3yr_avg < 0.5 &
                           !is.na(debt_equity_ratio_trend) & debt_equity_ratio_trend <= 0] <- 1
  debt_equity_ratio_flag[is.na(debt_equity_ratio_flag)] <- 0
  
  # price_52_week_low_flag <- NA
  # price_52_week_low_flag[!is.na(price_as_pct_of_year_range) & price_as_pct_of_year_range <= 0.25 & 
  #                          price_as_pct_of_year_range >= 0] <- 1
  # price_52_week_low_flag[is.na(price_52_week_low_flag)] <- 0
  
})

#######################################################################################
## COUNT THE NUMBER OF FLAGS EACH STOCK HAS
#######################################################################################

fulldata$n_flags <- apply(fulldata[grep("_flag", names(fulldata))], 1, sum, na.rm=TRUE)

test <- fulldata[fulldata$n_flags == 0, 
                 c(match("ticker", names(fulldata)),
                   grep("flag", names(fulldata)))]

test <- fulldata[fulldata$n_flags >= 6 & !is.na(fulldata$market_cap_category) & fulldata$market_cap_category %in% c("Mid", "Large"), 
                 c(match("ticker", names(fulldata)),
                   grep("flag", names(fulldata)))]





